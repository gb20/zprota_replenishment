class ZCL_WERKS_STAT_UPDATER  definition
  public abstract
  create protected .

public section.

  types:
    t_werks_stat_data_tt TYPE TABLE OF zprt_werks_stat WITH DEFAULT KEY .

  class-methods PERFORMANCE_TEST
    importing
      !I_ROWS type INT4
    raising
      ZCX_ERROR_MSG .
  class-methods UPDATE
    importing
      !IT_WERKS_STAT_UPD type T_WERKS_STAT_DATA_TT
    raising
      ZCX_ERROR_MSG .
protected section.

  methods DO_UPDATE ABSTRACT
    importing
      !IT_WERKS_STAT_UPD type T_WERKS_STAT_DATA_TT
    raising
      ZCX_ERROR_MSG .
private section.

  constants MC_MAX_SIMPLE_COUNT type INT4 value 100000 ##NO_TEXT.

  class-methods GET_INSTANCE
    importing
      !IV_COUNT type INT4
    returning
      value(EO_INSTANCE) type ref to ZCL_WERKS_STAT_UPDATER .
ENDCLASS.



CLASS ZCL_WERKS_STAT_UPDATER IMPLEMENTATION.


  method GET_INSTANCE.

    IF iv_count <= mc_max_simple_count.
      CREATE OBJECT eo_instance TYPE zcl_werks_stat_updater_simple.
    ELSE.
      CREATE OBJECT eo_instance TYPE zcl_werks_stat_updater_switch.
    ENDIF.

  endmethod.


  METHOD performance_test.

    DATA: lt_zprt_werks_stat TYPE TABLE OF zprt_werks_stat.

    SELECT * INTO TABLE lt_zprt_werks_stat
      FROM zprt_werks_stat
      UP TO i_rows ROWS.

    zcl_werks_stat_updater=>update( it_werks_stat_upd = lt_zprt_werks_stat ).
  ENDMETHOD.


  METHOD update.
    DATA: lo_updater TYPE REF TO zcl_werks_stat_updater.

    lo_updater = get_instance( iv_count = lines( it_werks_stat_upd ) ).
    lo_updater->do_update( it_werks_stat_upd ).
  ENDMETHOD.
ENDCLASS.
