class ZCL_WERKS_STAT_UPDATER_SWITCH definition
  public
  inheriting from ZCL_WERKS_STAT_UPDATER
  final
  create public .

public section.
protected section.

  methods DO_UPDATE
    redefinition .
private section.

  types:
    BEGIN OF t_sql_stmt,
           line(72),
         END OF t_sql_stmt .

  constants C_MAIN_TBL type DD03L-TABNAME value 'ZPRT_WERKS_STAT' ##NO_TEXT.
  constants C_TEMP_TBL type DD03L-TABNAME value 'ZPRT_WERKS_STATT' ##NO_TEXT.

  methods CREATE_TBL
    raising
      ZCX_ERROR_MSG .
  methods FILL_DATA
    importing
      !IT_WERKS_STAT_UPD type T_WERKS_STAT_DATA_TT
    raising
      ZCX_ERROR_MSG .
  methods CREATE_IDX
    raising
      ZCX_ERROR_MSG .
  methods SWITCH_TBL
    raising
      ZCX_ERROR_MSG .
  methods ENQ
    raising
      ZCX_ERROR_MSG .
  methods DEQ .
  methods CREATE_TBL_ABAP
    raising
      ZCX_ERROR_MSG .
  methods CREATE_TBL_DB
    raising
      ZCX_ERROR_MSG .
ENDCLASS.



CLASS ZCL_WERKS_STAT_UPDATER_SWITCH IMPLEMENTATION.


  METHOD create_idx.
    DATA: lt_dbindexes TYPE TABLE OF dbindex,
          lt_dbindflds TYPE TABLE OF dbindfld,
          lt_dbfldnam  TYPE TABLE OF dbfldnam,
          lv_new_idx   TYPE dd12l-dbindex,
          lt_stmt_tab  TYPE TABLE OF t_sql_stmt,
          lt_stmt_col  TYPE TABLE OF t_sql_stmt,
          lv_rc        TYPE sy-subrc.

    CALL FUNCTION 'DB_GET_INDEXES'
      EXPORTING
        tabname   = c_main_tbl
      TABLES
        dbindexes = lt_dbindexes
        dbindflds = lt_dbindflds.

    LOOP AT lt_dbindexes INTO DATA(ls_dbindexes).
      CLEAR: lt_dbfldnam, lt_stmt_tab.

      LOOP AT lt_dbindflds INTO DATA(ls_dbindflds) WHERE index = ls_dbindexes-name.
        APPEND ls_dbindflds-field TO lt_dbfldnam.
      ENDLOOP.

      lv_new_idx = ls_dbindexes-name.
      REPLACE c_main_tbl IN lv_new_idx WITH c_temp_tbl.

      CALL FUNCTION 'DB_CREATE_INDEX_S'
        EXPORTING
          dbindex                  = lv_new_idx
          tabname                  = c_temp_tbl
          unique                   = ls_dbindexes-unique
          no_exec                  = abap_true
          db_check_flag            = abap_true
        TABLES
          ddfldnames               = lt_dbfldnam
          statements               = lt_stmt_tab
        EXCEPTIONS
          index_exists             = 1
          index_not_created        = 2
          program_not_generated    = 3
          program_not_written      = 4
          table_does_not_exist     = 5
          storage_not_determinated = 6
          OTHERS                   = 7.

      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.

      IF sy-dbsys(3) = 'ORA'.
        LOOP AT lt_stmt_tab ASSIGNING FIELD-SYMBOL(<fs_stmt>).
          IF <fs_stmt>-line CS 'ENDEXEC.'.
            INSERT ` parallel (degree 5) NOLOGGING` INTO lt_stmt_tab INDEX sy-tabix.
            EXIT.
          ENDIF.
        ENDLOOP.
      ENDIF.

      APPEND LINES OF lt_stmt_tab TO lt_stmt_col.
    ENDLOOP.

    IF lt_stmt_col IS NOT INITIAL.
      CALL FUNCTION 'WRITE_AND_CALL_DBPROG'
        IMPORTING
          rc                  = lv_rc
        TABLES
          statements          = lt_stmt_col
        EXCEPTIONS
          program_not_written = 01
          sql_error_occured   = 02.

      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.
    ENDIF.

    COMMIT WORK.
  ENDMETHOD.


  METHOD CREATE_TBL.
    me->create_tbl_abap( ).
    me->create_tbl_db( ).
  ENDMETHOD.


  METHOD create_tbl_abap.
    DATA:
      ls_dd02v_template TYPE dd02v,
      ls_dd09l_template TYPE dd09l,
      lt_dd03p_template TYPE TABLE OF dd03p,

      ls_dd02v_current  TYPE dd02v,
      ls_dd09l_current  TYPE dd09l,
      lt_dd03p_current  TYPE TABLE OF dd03p,

      lv_ddobjname      TYPE ddobjname.

    lv_ddobjname = c_main_tbl.

    CALL FUNCTION 'DDIF_TABL_GET'
      EXPORTING
        name          = lv_ddobjname
      IMPORTING
        dd02v_wa      = ls_dd02v_template
        dd09l_wa      = ls_dd09l_template
      TABLES
        dd03p_tab     = lt_dd03p_template
      EXCEPTIONS
        illegal_input = 1
        OTHERS        = 2.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    ls_dd02v_template-tabname = c_temp_tbl.
    ls_dd09l_template-tabname = c_temp_tbl.

    LOOP AT lt_dd03p_template ASSIGNING FIELD-SYMBOL(<fs_dd03p>).
      <fs_dd03p>-tabname = c_temp_tbl.
    ENDLOOP.

    lv_ddobjname = c_temp_tbl.

    CALL FUNCTION 'DDIF_TABL_GET'
      EXPORTING
        name          = lv_ddobjname
      IMPORTING
        dd02v_wa      = ls_dd02v_current
        dd09l_wa      = ls_dd09l_current
      TABLES
        dd03p_tab     = lt_dd03p_current
      EXCEPTIONS
        illegal_input = 1
        OTHERS        = 2.

    CLEAR: ls_dd02v_current-as4user,
           ls_dd02v_current-as4date,
           ls_dd02v_current-as4time,
           ls_dd02v_template-as4user,
           ls_dd02v_template-as4date,
           ls_dd02v_template-as4time,
           ls_dd09l_current-as4user,
           ls_dd09l_current-as4date,
           ls_dd09l_current-as4time,
           ls_dd09l_template-as4user,
           ls_dd09l_template-as4date,
           ls_dd09l_template-as4time.

    IF lt_dd03p_current <> lt_dd03p_template OR
       ls_dd02v_current <> ls_dd02v_template OR
       ls_dd09l_current <> ls_dd09l_template.

      CALL FUNCTION 'DDIF_TABL_PUT'
        EXPORTING
          name              = lv_ddobjname
          dd02v_wa          = ls_dd02v_template
          dd09l_wa          = ls_dd09l_template
        TABLES
          dd03p_tab         = lt_dd03p_template
        EXCEPTIONS
          tabl_not_found    = 1
          name_inconsistent = 2
          tabl_inconsistent = 3
          put_failure       = 4
          put_refused       = 5
          OTHERS            = 6.

      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.

      CALL FUNCTION 'DDIF_TABL_ACTIVATE'
        EXPORTING
          name        = lv_ddobjname
          auth_chk    = ''
          prid        = -1
          excommit    = 'X'
        EXCEPTIONS
          not_found   = 1
          put_failure = 2
          OTHERS      = 3.
      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.
    ENDIF.
  ENDMETHOD.


  METHOD create_tbl_db.
    DATA: lv_rc       TYPE sy-subrc,
          lt_x031l    TYPE TABLE OF x031l,
          lt_ddfields TYPE TABLE OF ddfield,
          lv_text     TYPE bapiret2-message.

    CALL FUNCTION 'DB_EXISTS_TABLE'
      EXPORTING
        tabname = c_temp_tbl
      IMPORTING
        subrc   = lv_rc.

    IF lv_rc EQ 0.
      CALL FUNCTION 'DB_DROP_TABLE'
        EXPORTING
          tabname               = c_temp_tbl
        EXCEPTIONS
          program_not_generated = 1
          program_not_written   = 2
          table_not_dropped     = 3
          OTHERS                = 4.

      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.
    ENDIF.

    CALL FUNCTION 'DD_GET_NAMETAB'
      EXPORTING
        tabname   = c_temp_tbl
      TABLES
        x031l_tab = lt_x031l
      EXCEPTIONS
        OTHERS    = 3.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    CALL FUNCTION 'DD_MAP_NAMETAB_TO_DDFIELDS'
      TABLES
        ddfields  = lt_ddfields
        x031l_tab = lt_x031l
      EXCEPTIONS
        OTHERS    = 1.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    " Отключение главного индекса
    LOOP AT lt_ddfields ASSIGNING FIELD-SYMBOL(<ddfields>).
      IF <ddfields>-keyflag = 'X'.
        CLEAR <ddfields>-keyflag.
      ENDIF.
    ENDLOOP.

    CALL FUNCTION 'DB_CREATE_TABLE'
      EXPORTING
        tabname               = c_temp_tbl
      TABLES
        ddfields              = lt_ddfields
      EXCEPTIONS
        program_not_generated = 1
        program_not_written   = 2
        table_exists          = 3
        table_not_created     = 4
        OTHERS                = 5.

    IF sy-dbsys(3) = 'ORA'.
      TRY.
          cl_sql_connection=>get_connection( )->create_statement( )->execute_ddl( |ALTER TABLE { c_temp_tbl } NOLOGGING| ).
        CATCH cx_sql_exception INTO DATA(lo_sql_exception).
          lv_text = lo_sql_exception->get_text( ).
          zcx_error_msg=>raise_from_text( i_message = lv_text ).
      ENDTRY.
    ENDIF.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    COMMIT WORK.
  ENDMETHOD.


  method DEQ.
    DATA: lv_tabname TYPE rstable-tabname.

    lv_tabname = c_main_tbl.

    CALL FUNCTION 'DEQUEUE_E_TABLEE'
      EXPORTING
        mode_rstable   = 'E'
        tabname        = lv_tabname
        _scope         = '1'
      EXCEPTIONS
        foreign_lock   = 1
        system_failure = 2
        OTHERS         = 3.
  endmethod.


  METHOD do_update.
    me->enq( ).
    me->create_tbl( ).
    me->fill_data( it_werks_stat_upd ).
    me->create_idx( ).
    me->switch_tbl( ).
    me->deq( ).
  ENDMETHOD.


  METHOD enq.
    DATA: lv_tabname TYPE rstable-tabname.

    lv_tabname = c_main_tbl.

    CALL FUNCTION 'ENQUEUE_E_TABLEE'
      EXPORTING
        mode_rstable   = 'E'
        tabname        = lv_tabname
        _scope         = '1'
      EXCEPTIONS
        foreign_lock   = 1
        system_failure = 2
        OTHERS         = 3.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.
  ENDMETHOD.


  method FILL_DATA.
    data: lv_text type bapiret2-message.

    INSERT (c_temp_tbl) FROM TABLE it_werks_stat_upd.
    COMMIT WORK.

    TRY.
        cl_sql_connection=>get_connection( )->create_statement( )->execute_update(
          |INSERT /*+ APPEND */ INTO { c_temp_tbl } | &&
          |  SELECT * FROM { c_main_tbl } | &&
          |    WHERE NOT EXISTS (SELECT NULL FROM { c_temp_tbl } | &&
          |                        WHERE { c_temp_tbl }.MANDT = { c_main_tbl }.MANDT | &&
          |                          AND { c_temp_tbl }.MATNR = { c_main_tbl }.MATNR | &&
          |                          AND { c_temp_tbl }.WERKS = { c_main_tbl }.WERKS)| ).
        COMMIT WORK.
      CATCH cx_sql_exception into data(lo_sql_exception).
        lv_text = lo_sql_exception->get_text( ).
        zcx_error_msg=>raise_from_text( i_message = lv_text ).
    ENDTRY.
  endmethod.


  METHOD switch_tbl.
    DATA: lt_dbindexes TYPE TABLE OF dbindex,
          lv_old_idx   TYPE dd12l-dbindex,
          lv_new_idx   TYPE dd12l-dbindex,
          lv_text      TYPE bapiret2-message.

    CALL FUNCTION 'DB_DROP_TABLE'
      EXPORTING
        tabname               = c_main_tbl
      EXCEPTIONS
        program_not_generated = 1
        program_not_written   = 2
        table_not_dropped     = 3
        OTHERS                = 4.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    CALL FUNCTION 'DB_GET_INDEXES'
      EXPORTING
        tabname   = c_temp_tbl
      TABLES
        dbindexes = lt_dbindexes.

    CALL FUNCTION 'DB_RENAME_TABLE'
      EXPORTING
        tabname_old              = c_temp_tbl
        tabname_new              = c_main_tbl
      EXCEPTIONS
        new_table_exists         = 1
        old_table_does_not_exist = 2
        program_not_generated    = 3
        program_not_written      = 4
        OTHERS                   = 5.

    IF sy-subrc <> 0.
      zcx_error_msg=>raise_from_sy( ).
    ENDIF.

    LOOP AT lt_dbindexes INTO DATA(ls_dbindexes).
      lv_old_idx = ls_dbindexes-name.
      lv_new_idx = ls_dbindexes-name.
      REPLACE c_temp_tbl IN lv_new_idx WITH c_main_tbl.

      CALL FUNCTION 'DB_RENAME_INDEX'
        EXPORTING
          tabname                  = c_main_tbl
          indname_old              = lv_old_idx
          indname_new              = lv_new_idx
        EXCEPTIONS
          new_index_exists         = 1
          old_index_does_not_exist = 2
          program_not_generated    = 3
          program_not_written      = 4
          OTHERS                   = 5.

      IF sy-subrc <> 0.
        zcx_error_msg=>raise_from_sy( ).
      ENDIF.
    ENDLOOP.

    IF sy-dbsys(3) = 'ORA'.
      TRY.
          cl_sql_connection=>get_connection( )->create_statement( )->execute_ddl( |ALTER TABLE { c_main_tbl } LOGGING| ).
        CATCH cx_sql_exception INTO DATA(lo_sql_exception).
          lv_text = lo_sql_exception->get_text( ).
          zcx_error_msg=>raise_from_text( i_message = lv_text ).
      ENDTRY.
    ENDIF.

    COMMIT WORK.
  ENDMETHOD.
ENDCLASS.
